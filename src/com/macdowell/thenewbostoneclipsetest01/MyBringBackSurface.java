package com.macdowell.thenewbostoneclipsetest01;

import android.content.Context;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

//Setting up a thread to run within the runnable method
public class MyBringBackSurface extends SurfaceView implements Runnable{

	//New variable of type SurfaceHolder
	SurfaceHolder ourHolder;
	
	Thread ourThread = null;
	boolean isRunning = false;
	
	
	//Constructor
	public MyBringBackSurface(Context context) {
		super(context);
		
		//Checks to make sure the surface is valid
		ourHolder = getHolder();
		
		//Moved thread creation to resume() method 
		
	}

	//Need methods for pause, resume, etc
	
	/*
	 * Add a method for the thread to pause. How it works is that if 
	 * the "isRunning" is false (IE, pressed home or back), it will try to
	 * join the thread (Similar to ourThread.destroy()), which pauses it so
	 * nothing else can interact. It then breaks the loop and sets the 
	 * ourThread variable to null.
	 */
	public void pause(){
		isRunning = false;
		while(true){
			try {
				ourThread.join();
			} catch (InterruptedException e01) {
				e01.printStackTrace();
			}
			break;
		}
		ourThread = null;
	}
	
	/*
	 * Add a method for resume (back and in again). How this works is when
	 * it is started or resumed, it creates a new thread and then starts it.
	 * 
	 */
	public void resume(){
		isRunning = true;
		//Defines it within the context of this class
		ourThread = new Thread(this);
		//Starts the thread
		ourThread.start();
	}
	
	
	
	@Override
	//Create a holder, which manages surface
	//Allow us to lock the canvas so no other thread can draw on the canvas while one is
	public void run() {
		
		while(isRunning){
		
			//First check to see if the surface is valid. We WANT it to skip continue as continue breaks the loop like break
			if(!ourHolder.getSurface().isValid())
				continue;
			
			//Canvas locks it (IE working in room, lock door so no one can come in during)
			Canvas canvas = ourHolder.lockCanvas();
			
			//Paint on the canvas now that it is locked. Paints background
			canvas.drawRGB(200, 88, 147);
			
			//Since we are done with painting, unlock the canvas and post it.
			ourHolder.unlockCanvasAndPost(canvas);
		}
		
		
		
	}

}
