package com.macdowell.thenewbostoneclipsetest01;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class GraphicsSurface extends Activity implements OnTouchListener{

	MyBringBackSurface ourSurfaceView;
	
	//These 2 values are the x/y location on the screen via coords
	float x, y;
	
	
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		ourSurfaceView = new MyBringBackSurface(this);
		
		//Makes it so the ourSurfaceView is accessible by the entire canvas
		ourSurfaceView.setOnTouchListener(this);
		
		//Set x and y = 0 here as opposed to global variables
		x = 0;
		y = 0;
		
		setContentView(ourSurfaceView);
				
	}
	
	//On pause method and resume method, used in MyBringBackSurface Java
	@Override
	protected void onPause() {
		
		super.onPause();
		ourSurfaceView.pause();
		
	}
	
	protected void onResume(){
		
		super.onResume();
		ourSurfaceView.resume();
		
	}

	//When the screen is touched with the individual icon
	//We want the touch event to be accessible to the entire canvas (ourSurfaceView)
	//When the screen is touched, it calls this method
	@Override
	public boolean onTouch(View arg0, MotionEvent event) {
		
		//Define the X by returning the X coord from where it was clicked
		x = event.getX();
		//Define the Y by returning the Y coord from where it was clicked
		y = event.getY();
		
		return false;
	}
}
