package com.macdowell.thenewbostoneclipsetest01;

import java.util.Random;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

/*
 * This class creates a field in which people can type passwords,
 * type information, give commands, and click a button to confirm if
 * a command worked (regarding location and placement).
 */
public class TextPlay extends Activity implements View.OnClickListener{
	
	//Global Variables
	Button checkCommand;
	ToggleButton passwordToggle;
	EditText input;
	TextView displayText;
		
	//Initializer for the global variables. Bridges IDs to the XML
	private void Initializer_Method_For_Buttons() {
		
		//The 4 buttons/ icons. Reference Video 025 for information
		checkCommand = (Button) findViewById(R.id.button_results);
		passwordToggle = (ToggleButton) findViewById(R.id.toggleButtonPassword);
		input = (EditText) findViewById(R.id.editTextCommands);
		displayText = (TextView) findViewById(R.id.text_view_results);
	}//End Initializer_Method_For_Buttons method
			
	@Override
	//The "Main" Method here
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		setContentView(R.layout.text);

		Initializer_Method_For_Buttons();
		
		//Setup an onclicklistener (nested-style setup) for passwordToggle
		passwordToggle.setOnClickListener(this);
		
		//Setup CheckCommand
		checkCommand.setOnClickListener(this);
				
	}//End On create Method



	@Override
	//This method
	public void onClick(View view) {
		
		switch(view.getId())
		{
		case R.id.button_results:
			
			//Gets the text from the box (getText()) and then converts it to a string (toString)
			//This is the data in the box entered. When the checkCommand button is clicked, the string is a result
			String check = input.getText().toString();
			
			displayText.setText(check);
			
			if (check.contentEquals("left"))
			{
				displayText.setGravity(Gravity.LEFT);
			} else if (check.contains("center")){
				displayText.setGravity(Gravity.CENTER);
			} else if (check.contains("right")) {
				displayText.setGravity(Gravity.RIGHT);
			} else if (check.contains("blue")) {
				displayText.setTextColor(Color.BLUE);
			} else if (check.contains("WTF") || check.contains("wtf")){
				Random myRandom = new Random();
				displayText.setText("WTF...");
				
				//Random pixel size from 1-75
				displayText.setTextSize(myRandom.nextInt(75));
				
				//Random Color, RGB random 1-260
				displayText.setTextColor(Color.rgb(myRandom.nextInt(265), myRandom.nextInt(265), myRandom.nextInt(265)));
				
				//
				switch(myRandom.nextInt(3))
				{
				case 0:
					displayText.setGravity(Gravity.LEFT);
					break;
				case 1:
					displayText.setGravity(Gravity.CENTER);
					break;
				case 2:
					displayText.setGravity(Gravity.RIGHT);
					break;
				}
				
					
			} else {
				//Sets info back to default
				displayText.setText("Invalid");
				displayText.setGravity(Gravity.CENTER);
				displayText.setTextColor(Color.WHITE);
			}
			
			break;
		case R.id.toggleButtonPassword:
			if (passwordToggle.isChecked())
			{
				//If boolean is true and it is checked, make the text not visible, IE like a password
				input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			} else {
				//If boolean is false, regular text
				input.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		}
		
		
	}//End onClick method
	
	

	
}
