package com.macdowell.thenewbostoneclipsetest01;

import java.util.Scanner;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

public class OpenedClass extends Activity implements View.OnClickListener, OnCheckedChangeListener {

	//Global Variables
	TextView question, test;
	Button returnData;
	String gotBread, setData;
	
	//References the radio group buttons
	RadioGroup selectionList;
	
	//Initializes all buttons, views, etc
	private void initialize(){
		
		question = (TextView) findViewById(R.id.tvQuestion);
		test = (TextView) findViewById(R.id.tvText);
		returnData = (Button) findViewById(R.id.bReturn);
		returnData.setOnClickListener(this);
		//The order of the next 2  matters greatly!!
		selectionList = (RadioGroup) findViewById(R.id.rgAnswers);		
		selectionList.setOnCheckedChangeListener(this);

	}
		
	@Override
	//Main
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send);
		initialize();
		
		//Check video 058 if any errors pop up
		//This entire section (next 8ish lines) is about changing stored values
		SharedPreferences getData = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		
		String et = getData.getString("name", "Default");
		
		String values = getData.getString("list", "4");
		
		if(values.contentEquals("1")){
			question.setText(et);
		}
		
		
		
		//IMPORTANT!!!  <----------------------
		/*
		 * Commenting out these 3 lines of code because they will give
		 * an error if the 'other' button (results) is pressed due to 
		 * there being 2 bundles. If you want the other one to work, 
		 * you will need to uncomment these and recomment out the lines
		 * in the public void onClick(View v) method. 
		
		Bundle gotBasket = getIntent().getExtras();
		
		//Remember, Key is the file name from the variable from the data.java class
		gotBread = gotBasket.getString("key");
		
		question.setText(gotBread);
		 */
	}

	@Override
	//OnClickListener, when return button is clicked, data is passed (basket/ bread)
	public void onClick(View v) {
		//Setup an intent first
		Intent person = new Intent();
		
		//Bundle is what the Intent is carrying
		Bundle backpack = new Bundle();
		
		//Passing the setData from the method below
		backpack.putString("answer", setData);
		
		//
		person.putExtras(backpack);
		
		//Starting activity FOR a result, this gives it back
		//@Params, 1st is everything checks out, 2nd is intent
		setResult(RESULT_OK, person); //Turns blue because we did not write the method, built into API
		
		//Start it and go
		finish();
		
	}

	@Override
	//OnCheckedChagned (Radion button)
	public void onCheckedChanged(RadioGroup arg0, int arg1) {
		
		//Setup a switch case to see which radio button is pressed
		switch(arg1){
		
		case R.id.rCrazy:
			setData = "Probably a true statement";
			break;
			
		case R.id.rSexy:
			setData = "If you want to think of it that way I guess";
			break;
			
		case R.id.rBoth:
			setData = "Probably because of all this programming...";
			break;
		}
		test.setText(setData);
	}

	
	
	
	
}