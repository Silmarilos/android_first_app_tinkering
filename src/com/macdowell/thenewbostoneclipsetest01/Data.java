package com.macdowell.thenewbostoneclipsetest01;

import java.util.Scanner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class Data extends Activity implements View.OnClickListener {

	Button start, startFor;
	EditText sendET;
	TextView gotAnswer;
	
	//Initializes all buttons, views, etc
	private void initialize(){
		
		start = (Button) findViewById(R.id.bSA);
		startFor = (Button) findViewById(R.id.bSAFR);
		sendET = (EditText) findViewById(R.id.etSend);
		gotAnswer = (TextView) findViewById(R.id.tvGot);
		
		start.setOnClickListener(this);
		startFor.setOnClickListener(this);
	}
		
	@Override
	//Main
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.get);
		initialize();
		//
		
	}

	@Override
	//OnClickListener
	public void onClick(View arg0) {
		
		switch (arg0.getId()){
		
		case R.id.bSA:
			
			//String will be received from the edit text window
			String bread = sendET.getText().toString();
			
			//Bundle object for passing data between classes
			Bundle basket = new Bundle();
			
			//Placing items into the bundle (String)
			//The Key is the string name
			basket.putString("key", bread);
			
			//Intent opens another class (called OpenClass)
			//@Params, Context from and then class to open
			Intent intent1 = new Intent(Data.this, OpenedClass.class);
			
			//Carry this basket with you computer
			intent1.putExtras(basket);
			
			startActivity(intent1);
			break;
			
		case R.id.bSAFR:
			
			//Setup new intent first
			Intent intent2 = new Intent(Data.this, OpenedClass.class);
			
			//
			startActivityForResult(intent2, 0);
			
			
			//
			break;
		}
		
	}

	@Override
	//
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		//@Param, Resultcode is listed above passing from OpenedClass class
		if (resultCode == RESULT_OK) {
			//Bundle is from the Data.class passed in
			Bundle basket = data.getExtras();
			
			String str1 = basket.getString("answer");
			
			//This is from the textView
			gotAnswer.setText(str1);
			
		}
	}

	
	
	
	
}