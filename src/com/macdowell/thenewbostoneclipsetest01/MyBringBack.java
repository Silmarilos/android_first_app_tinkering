package com.macdowell.thenewbostoneclipsetest01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.view.View;

public class MyBringBack extends View{

	Bitmap fArrow;
	
	//Animation variable
	float changingY;
	
	//This is the font variable
	Typeface font;
	
	//Constructor
	public MyBringBack(Context context) {
		super(context);
	
		//Setting the bitmap variable = to the flaming_arrow icon, listed in drawable-hdpi.
		fArrow = BitmapFactory.decodeResource(getResources(), R.drawable.flaming_arrow);
		
		//creates a font by referencing the context of THIS class and the pathway, lucinda
		font = Typeface.createFromAsset(context.getAssets(), "lucinda.ttf");
		changingY=0;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		
		super.onDraw(canvas);
		
		//Setting background color
		canvas.drawColor(Color.WHITE);
		
		//Setup the font stuff
		Paint textPaint = new Paint();
		textPaint.setColor(Color.RED);
		textPaint.setTextAlign(Align.CENTER);
		textPaint.setTextSize(38);
		//Typeface of font included here to change font
		textPaint.setTypeface(font);
		
		//Add the font to the canvas. @ Params: String, float, float, paint.
		canvas.drawText("---Working With Portals!", (float) ((canvas.getWidth())/2.4) , 200, textPaint);
		
		//@param Bitmap, width, height (0 is at top), null
		canvas.drawBitmap(fArrow, (float) ((canvas.getWidth())/2.4), changingY, null);
		
		//If the variable changingY is less than the height of the canvas...
		if (changingY < canvas.getHeight()){
			//Increase the size by 10 pixels until it goes to else
			changingY += 10;
		} else {
			changingY = 0;
		}
		
		//Rect = Rectangle class
		Rect midRect = new Rect();
		
		//Dimensions of rectangle
		midRect.set(0, 400, canvas.getWidth(), 550);
		
		//Paint Variable
		Paint ourBlue = new Paint();
		
		//Sets the color to blue within the rectangle
		ourBlue.setColor(Color.BLUE);
		
		//Add to canvas. @Param, first is rectangle, second is background color
		canvas.drawRect(midRect, ourBlue);
		
		/*
		 * This is essentially like writing a while loop. It invalidates the
		 * entire above if else statement and then runs it again
		 */
		invalidate();
		
	}
	
	
	
}
