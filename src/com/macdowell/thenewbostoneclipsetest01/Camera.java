package com.macdowell.thenewbostoneclipsetest01;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class Camera extends Activity implements View.OnClickListener{
	
	ImageButton image_Button;
	Button button1;
	ImageView image_View;
	Intent intent1;
	Bitmap bmp;
	
	//This is a variable for return data from the camera. Note that it is an int
	final static int camera_Data = 0;
	
	//Initializes all buttons, views, etc
	private void initialize(){
		
		image_Button = (ImageButton) findViewById(R.id.image_Button_Take_Picture);
		image_View = (ImageView) findViewById(R.id.image_View_Returned_Pic);
		button1 = (Button) findViewById(R.id.button_Set_Wallpaper);
				
		button1.setOnClickListener(this);
		image_Button.setOnClickListener(this);	

	}
	
	//
	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		//Switch to choose which button they click
		switch (v.getId()){
		
		//Change code here to set as something else
		case R.id.button_Set_Wallpaper:
		
			try {
				//No clue why it is crossed out...
				getApplicationContext().setWallpaper(bmp);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case R.id.image_Button_Take_Picture:
			
			//This accesses the 'MediaStore' which houses the info for taking pictures
			intent1 = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			
			//We want to start the activity, but also to return data
			//@ Parameters, 1st passes in intent, second is variable return data is stored in
			startActivityForResult(intent1, camera_Data);
			
			break;
		}
		
	}
	
	//
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photo);
		initialize(); 
		
		//Initialize bitmap via input stream
		InputStream is = getResources().openRawResource(R.drawable.blank);
		
		//We need to de-code the input stream before setting it
		bmp = BitmapFactory.decodeStream(is);
	}

	@Override
	//Use method from activity class called onActivityResult to get data and check it
	//@Params, request code, result code, intent to pass in
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		//"If result comes out ok..."
		if (resultCode == RESULT_OK){
			Bundle extras = data.getExtras();
			
			//Create the bitmap in global vars
			bmp = (Bitmap) extras.get("data");
			
			image_View.setImageBitmap(bmp);
			
		}
		
	}


	
	
	
}
