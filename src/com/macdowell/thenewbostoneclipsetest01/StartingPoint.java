package com.macdowell.thenewbostoneclipsetest01;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*
 * This class creates an icon that holds a number. The number can be 
 * incremented or decremented depending on whether the user clicks the 
 * add one or subtract one buttons. It keeps a running counter.
 */
public class StartingPoint extends Activity {

	int counter;
	Button add, sub; //Imported Button Package
	TextView display; //Imported TextView Package
	
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        counter = 0;
        //These 3 are in the XML file. Similar to <Header> </Header> HTML structure
        add = (Button) findViewById(R.id.bAdd);
        sub = (Button) findViewById(R.id.bSub);
        display = (TextView) findViewById(R.id.tvDisplay);
        
        add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				counter++;
				display.setText("Your Total is: " + counter);
			}
		});
        
        sub.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				counter--;
				display.setText("Your Total is: " + counter);
			}
		});
           
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.starting_point, menu);
        return true;
    }
    
}
